from django.urls import path
from . import views

app_name = "myweb"
#url for app
urlpatterns = [
    path('', views.homePage, name= 'homePage'),
    path('portofolio/', views.portofolio, name= 'portofolio')
]
